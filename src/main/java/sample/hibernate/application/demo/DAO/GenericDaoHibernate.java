package sample.hibernate.application.demo.DAO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class GenericDaoHibernate <Entity> {
    protected Class<?> entityClass;

    @PersistenceContext
    protected EntityManager entityManager;

    public GenericDaoHibernate(Class<?> entityClass){
        this.entityClass = entityClass;
    }

    public Entity save(Entity entity){
        return entityManager.merge(entity);
    }

    public void delete(Object entity){
        entityManager.remove(entity);
    }

    public Entity findById(Long id){
        return (Entity) entityManager.find(entityClass,id);
    }



}
