package sample.hibernate.application.demo.DAO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sample.hibernate.application.demo.entity.SampleEntity;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;


@Repository
public class SampleDAO extends GenericDaoHibernate<SampleEntity> {

    Logger LOG = LoggerFactory.getLogger(SampleDAO.class);


    public SampleDAO(){
        super(SampleEntity.class);
    }

    public List<SampleEntity> findAllEntity(){
        return entityManager.createQuery("Select se from SampleEntity se").getResultList();
    }

    @Transactional
    public SampleEntity updateSampleEntity(SampleEntity sampleEntity) {
        try{
            return entityManager.merge(sampleEntity);

        }
        finally{
            if(null != entityManager){
                entityManager.close();
            }
        }
    }


    @Transactional
    public void deleteSample(Integer id){
        List<SampleEntity> result=null;
        try{
            StringBuilder sb = new StringBuilder();
            sb.append("DELETE from SampleEntity se where se.id= :id ");
            Query query = entityManager.createNativeQuery(sb.toString());
            query.setParameter("id",id);
            query.executeUpdate();

        }
        catch(NoResultException ex){
            LOG.error("No record Found!");
            throw ex;
        }
        finally {
            if(null != entityManager){
                entityManager.close();
            }
        }
    }


}
