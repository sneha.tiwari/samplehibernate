package sample.hibernate.application.demo.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sample.hibernate.application.demo.DAO.SampleDAO;
import sample.hibernate.application.demo.entity.SampleEntity;

import java.util.ArrayList;
import java.util.List;

@Service
public class SampleHibernateService {

    Logger logger= LoggerFactory.getLogger(SampleHibernateService.class);
    @Autowired
    private SampleDAO sampleDAO;

    public void insertData(){
        SampleEntity sampleEntity=new SampleEntity();
        sampleEntity.setName("Sneha");
        sampleDAO.updateSampleEntity(sampleEntity);

        SampleEntity sampleEntity1=new SampleEntity();
        sampleEntity1.setName("Vishal");
        sampleDAO.updateSampleEntity(sampleEntity1);
    }
    public void selectData(){
        List<SampleEntity> saveData=new ArrayList<>();
        saveData=sampleDAO.findAllEntity();
        logger.info("Data saved is:{}",saveData);
    }

}
