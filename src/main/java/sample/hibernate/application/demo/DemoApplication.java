package sample.hibernate.application.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import sample.hibernate.application.demo.impl.SampleHibernateService;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan
public class DemoApplication  implements CommandLineRunner {

    @Autowired
    private  SampleHibernateService hibernateService;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);

	}

    @Override
    public void run(String...args) throws Exception {
        hibernateService.insertData();
        hibernateService.selectData();

    }



}
